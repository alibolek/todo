# Flask-Todolist


Flask-Todolist is a simple To Do List web application.

---

### Docker
Using `docker-compose` you can simple run:

    docker-compose build
    docker-compose up

And the application will run on http://localhost:5000/

### Manually
If you prefer to run it directly on your local machine, I suggest using
[venv](https://docs.python.org/3/library/venv.html).

    pip install -r requirements.txt
    FLASK_APP=todolist.py flask run
