import unittest
from app import create_app, db
from app.models import TodoList


class TodolistTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app("testing")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

        self.todo_description = "shopping list"
        self.read_todo_description = "Read a book about TDD"

    def tearDown(self):
        db.drop_all()
        self.app_context.pop()

    def test_adding_todo_to_todolist(self):
        todolist = TodoList(
            description=self.todo_description,
            id = 1,
        ).save()

        self.assertEqual(todolist.description, self.todo_description)

