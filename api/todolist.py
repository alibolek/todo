from app import create_app

app = create_app("development")


@app.cli.command()
def test():
    print('tests')
    import sys
    import unittest

    tests = unittest.TestLoader().discover("tests")
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.errors or result.failures:
        sys.exit(1)
