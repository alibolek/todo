"""empty message

Revision ID: eff90419b076
Revises: None
Create Date: 2016-09-24 20:15:22.786236

"""

# revision identifiers, used by Alembic.
revision = "eff90419b076"
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        "todolist",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("description", sa.String(length=128), nullable=True),
    )

    ### end Alembic commands ###


def downgrade():
    op.drop_table("todolist")
