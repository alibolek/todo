from flask import abort, request, jsonify

from app.api import api
from app.models import TodoList


@api.route("/todolist/", methods=["POST"])
def add_todolist():
    try:
        todolist = TodoList(description=request.json.get("description"), id=request.json.get("id")).save()
    except:
        abort(400)
    return todolist.to_dict(), 201


@api.route("/todolist/", methods=["GET"])
def get_todolists():
    todolists = TodoList.query.all()
    response = jsonify({"todolists": [todolist.to_dict() for todolist in todolists]})
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response
