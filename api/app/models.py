from app import db


class BaseModel:

    def __commit(self):
        """Commits the current db.session, does rollback on failure."""
        from sqlalchemy.exc import IntegrityError

        try:

            db.session.commit()
        except IntegrityError:
            db.session.rollback()

    def delete(self):
        db.create_all()
        db.session.delete(self)
        self.__commit()

    def save(self):
        db.session.add(self)
        self.__commit()
        return self


class TodoList(db.Model, BaseModel):
    __tablename__ = "todolist"
    description = db.Column('description', db.String(128))
    id = db.Column('id', db.Integer, primary_key=True)

    def __init__(self, description, id):
        self.description = description
        self.id = id

    def to_dict(self):
        return {
            "description": self.description,
            "id": self.id
        }