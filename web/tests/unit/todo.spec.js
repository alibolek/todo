import { shallowMount, mount } from "@vue/test-utils";
import TodoList from "@/components/TodoList.vue";
import moxios from "moxios";
import flushPromises from "flush-promises";
describe("The Todo.vue component", () => {
  beforeEach(function () {
    moxios.install();
  });

  afterEach(function () {
    moxios.uninstall();
  });
  it("Can be mounted", () => {
    const wrapper = shallowMount(TodoList);
    expect(wrapper.exists()).toBeTruthy();
  });

  it("allows for adding one todo item", async () => {
    const wrapper = mount(TodoList);

    moxios.stubRequest("/api/todolist/", {
      status: 200,
      response: {
        data: [
          { id: 1, description: "title1" },
          { id: 2, description: "title2" },
        ],
      },
    });
    wrapper.find("#add-todo-input").setValue("Hello Modanisa");
    wrapper
      .find(".add-todo-button")
      .trigger("click")
      .then(() => {
        wrapper.vm.getAllTodosFromDb().then(() => {
          expect(wrapper.find("#todo-item").text()).toContain("Hello Modanisa");
        });
      });
  });
});
